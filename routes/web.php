<?php

use App\Http\Controllers\PostinganController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KomentarController;
use App\Http\Controllers\Admin\PenggunaController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\PostinganController as AdminPostinganController;
use App\Http\Controllers\Auth\AdminLoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/home/search', [HomeController::class, 'search'])->name('search');
Route::get('/profile/{id}', [UserController::class, 'index'])->name('profile');
Route::get('/profile/edit/{id}', [UserController::class, 'edit'])->name('profile.edit');
Route::post('/profile/update', [UserController::class, 'update'])->name('profile.update');
Route::get('/profile/follow/{id}', [UserController::class, 'follow'])->name('profile.follow');
Route::get('/profile/unfollow/{id}', [UserController::class, 'unfollow'])->name('profile.unfollow');
Route::get('/postingan/create', [PostinganController::class, 'index'])->name('postingan.create');
Route::post('/postingan/store', [PostinganController::class, 'store'])->name('postingan.store');
Route::get('/postingan/detail/{id}', [PostinganController::class, 'detail'])->name('postingan.detail');
Route::post('/postingan/komentar', [PostinganController::class, 'komen'])->name('postingan.komentar');


// ROUTE ADMIN
Route::prefix('admin')->group(function() {
    // LOGIN
    Route::get('/', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('/login', [AdminLoginController::class, 'index'])->name('admin.login');
    Route::post('/login', [AdminLoginController::class, 'login'])->name('admin.login.submit');
    Route::get('/logout', [AdminLoginController::class, 'logout'])->name('admin.logout');
    // PENGGUNA
    Route::get('/pengguna', [PenggunaController::class, 'index'])->name('admin.pengguna');
    Route::get('/pengguna/detail/{id}', [PenggunaController::class, 'detail'])->name('admin.pengguna.detail');
    Route::get('/pengguna/create', [PenggunaController::class, 'create'])->name('admin.pengguna.create');
    Route::post('/pengguna/store', [PenggunaController::class, 'store'])->name('admin.pengguna.store');
    Route::get('/pengguna/edit/{id}', [PenggunaController::class, 'edit'])->name('admin.pengguna.edit');
    Route::post('/pengguna/update', [PenggunaController::class, 'update'])->name('admin.pengguna.update');
    Route::get('/pengguna/delete/{id}', [PenggunaController::class, 'delete'])->name('admin.pengguna.delete');
    // POSTINGAN
    Route::get('/postingan', [AdminPostinganController::class, 'index'])->name('admin.postingan');
    Route::get('/postingan/detail/{id}', [AdminPostinganController::class, 'detail'])->name('admin.postingan.detail');
    Route::get('/postingan/delete/{id}', [AdminPostinganController::class, 'delete'])->name('admin.postingan.delete');
    Route::get('/postingan/delete_komentar/{id}', [AdminPostinganController::class, 'delete_komentar'])->name('admin.postingan.delete_komentar');
    
}) ;