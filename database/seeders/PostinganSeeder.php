<?php

namespace Database\Seeders;

use App\Models\Postingan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostinganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Postingan::create([
            'foto_postingan' => 'foto.jpg',
            'caption' => 'Foto pertama di Instagram :)',
            'likes' => 69,
            'id_poster' => 1,
        ]);
        Postingan::create([
            'foto_postingan' => 'foto.jpg',
            'caption' => 'Foto pertama di Instagram 2:)',
            'likes' => 22,
            'id_poster' => 2,
        ]);
        Postingan::create([
            'foto_postingan' => 'foto.jpg',
            'caption' => 'Foto pertama di Instagram 3:)',
            'likes' => 33,
            'id_poster' => 3,
        ]);
    }
}
