<?php

namespace Database\Seeders;

use App\Models\Follow;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FollowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $follows = [
            ['id_pengguna' => 1, 'id_following' => 2],
            ['id_pengguna' => 1, 'id_following' => 3],
            ['id_pengguna' => 2, 'id_following' => 1],
            ['id_pengguna' => 2, 'id_following' => 3],
            ['id_pengguna' => 3, 'id_following' => 1],
            ['id_pengguna' => 3, 'id_following' => 2],

        ];

        foreach($follows as $item){
            Follow::create(
                [
                    'id_pengguna' => $item['id_pengguna'],
                    'id_following' => $item['id_following'],
                ]
            );
        }

    }
}
