@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card mt-4"> 
            <div class="card-header d-flex flex-row align-items-center "> 
                <span class="ms-2 fw-bold">Edit Profil</span> 
            </div> 
            <div class="card-body"> 
              <div class="container">
                <form action="../update" method="post" autocomplete="off" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="id" id="id" value="{{$user->id}}" readonly>
                  <div class="row mb-2">
                      <div class="col-6">
                          <div class="form-group">
                              <label for="username">Username</label>
                              <input type="text" name="username" id="username" class="form-control" value="{{$user->username}}" readonly>
                          </div>
                      </div>
                      <div class="col-6">
                          <div class="form-group">
                              <label for="name">Nama</label>
                              <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama Anda" value="{{$user->name}}">
                              @error('name')
                                  <span class="text-danger">{{ $message }}</span>
                              @enderror
                          </div>
                      </div>
                  </div>
                  <div class="row mb-2">
                      <div class="col">
                          <div class="form-group">
                              <label for="bio">Bio</label>
                              <textarea name="bio" id="bio" cols="30" rows="2" class="form-control  @error('bio') is-invalid @enderror">{{$user->bio}}</textarea>
                              @error('bio')
                                  <span class="text-danger">{{ $message }}</span>
                              @enderror
                          </div>
                      </div>
                  </div>
                  <div class="row mb-2">
                      <div class="col-6">
                          <div class="form-group">
                              <label for="password">Password Baru</label>
                              <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
                              @error('password')
                                  <span class="text-danger">{{ $message }}</span>
                              @enderror
                          </div>
                      </div>
                      <div class="col-6">
                          <div class="form-group">
                              <label for="foto_profil">Foto Profil</label>
                              <input type="file" name="foto_profil" id="foto_profil" class="form-control @error('foto_profil') is-invalid @enderror">
                              @error('foto_profil')
                                  <span class="text-danger">{{ $message }}</span>
                              @enderror
                          </div>
                      </div>
                  </div>
                  <button class="btn btn-primary btn-sm mt-2">Simpan</button>
                  <a href="/admin/pengguna" class="btn btn-outline-secondary btn-sm mt-2">Batal</a>
              </form>
              </div>
            </div> 
        </div>
    </div>
</div>
@endsection
