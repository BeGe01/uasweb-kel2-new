@extends('layouts.app')

@section('content')
<div class="container" style="width: 70%;">
    <div class="row justify-content-center mb-5">
        <div class="col-md-4 text-center">
            <img class="rounded-circle" src="{{ asset("storage/profile/".$user->foto_profil )}}" alt="" width="180" height="180" />
        </div>
        <div class="col-md-8">
          <div class="d-flex align-items-center my-3">
            <h2>{{ $user->username }}</h2>
            @if($user->id == auth()->user()->id)
              <a href="/profile/edit/{{ $user->id }}" class="ms-2 btn btn-sm btn-outline-secondary">edit profile</a>
            @else
              @if($user->isFollowing($user->id))
                <a href="/profile/follow/{{ $user->id }}" class="ms-2 btn btn-sm btn-success">FOLLOW</a>
              @else
              <a href="/profile/unfollow/{{ $user->id }}" class="ms-2 btn btn-sm btn-danger">UNFOLLOW</a>
              @endif
            @endif
          </div>
          <div class="d-flex align-items-center">
            <span><strong>{{ $posts->count() }}</strong> Postingan</span>
            <span data-bs-toggle="modal" data-bs-target="#modal_followers" class="mx-3"><strong>{{ $followers }}</strong> Pengikut</span>
            <span data-bs-toggle="modal" data-bs-target="#modal_following"><strong>{{ $following }}</strong> Diikuti</span>
          </div>
          <div class="d-flex flex-column my-3">
            <strong>{{ $user->name }}</strong>
            <span>{{ $user->bio ?? '-' }}</span>
          </div>
        </div>
    </div>

    <hr>
    <h1 class="text-center">Postingan</h1>
    <hr>
    <div style="display: grid; grid-template-columns: repeat(3,1fr); gap: 30px;">
    @foreach ($posts as $index => $post)
      <a href=" {{ route('postingan.detail', ['id' => $post->id]) }} ">
        <img src="{{ asset("storage/post/" . $post->foto_postingan)}}" alt="" style="width: 100%" />
      </a>
    @endforeach
    </div>
</div>
<!-- Modal Followers -->
<div class="modal fade" id="modal_followers" tabindex="-1" aria-labelledby="foto_profil" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Daftar Followers</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <table class="table table-sm table-hover">
                  <thead>
                      <tr>
                          {{-- <th>No</th> --}}
                          <th>Username</th>
                      </tr>
                  </thead>
                  @foreach ($list_follower as $index => $follower)
                  <tr>
                      {{-- <td>{{$index+1}}</td> --}}
                      <td><a style="text-decoration: none;" href="./{{ $follower->id_pengguna }}">{{ $follower->username($follower->id_pengguna) }}</a></td>
                  </td>
                  @endforeach
              </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>
<!-- Modal Following -->
<div class="modal fade" id="modal_following" tabindex="-1" aria-labelledby="foto_profil" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Daftar Following</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <table class="table table-sm table-hover">
                  <thead>
                      <tr>
                          {{-- <th>No</th> --}}
                          <th>Username</th>
                      </tr>
                  </thead>
                  @foreach ($list_following as $index => $following)
                  <tr>
                      {{-- <td>{{$index+1}}</td> --}}
                      <td><a style="text-decoration: none;" href="./{{ $following->id_following }}">{{ $following->username($following->id_following) }}</a></td>
                  </td>
                  @endforeach
              </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>
@endsection
