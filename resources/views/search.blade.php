@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          @foreach ($users as $index => $user)
          <a href="/profile/{{ $user->id }}" style="text-decoration: none !important; color:black !important">
            <div class="card my-2">
              <div class="card-body">
                <div class="d-flex pb-2">
                  <div style="width: 10%;">
                    <img class="rounded-circle" src="{{ asset("storage/profile/" . $user->foto_profil) }}" alt="" width="50" height="50" /> 
                  </div>
                  <div style="width: 90%;">
                    <div class="fw-bold me-1">{{ $user->name }}</div>
                    <div style="font-size: .8rem;">@ {{ $user->username }}</div>
                    <div>{{ $user->bio ?? '-' }}</div>
                  </div>
                </div>
              </div>
            </div>
          </a>
          @endforeach
        </div>
    </div>
</div>
@endsection
