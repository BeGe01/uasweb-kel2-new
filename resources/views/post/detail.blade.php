@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card mt-4"> 
            <div class="card-header d-flex flex-row align-items-center "> 
                <img src="{{ asset("storage/profile/" . $user->foto_profil) }}" alt="" width="30" height="30" /> 
                <span class="ms-2">{{ $user->username }}</span> 
            </div> 
        
            <div class="card-body text-center"> 
                <img src="{{ asset("storage/post/" . $post->foto_postingan) }}" width="100%" alt="" /> 
        
                {{-- <div class="d-flex flex-column text-start"> 
                    <span>Disukai oleh <span style="font-weight: bolder">{{ $post->likes }} Orang</span></span> 
                </div>  --}}
        
                <div class="d-flex flex-row text-start"> 
                    <strong>{{ $user->username }}</strong> 
                    <span class="ms-2">{{ $post->caption }}</span> 
                  </div> 

                  <div class="d-flex flex-column text-start"> 
                    <span style="font-size: 12px">{{ date('d M Y', strtotime($post->created_at)) }}</span>
                  </div>
        
                <div class="d-flex flex-column text-start text-secondary mt-2"> 
                  @foreach ($comments as $index => $comment)
                    <div class="row my-2">
                      <div class="col-sm-1">
                        <img class="rounded-circle" src="{{ asset("storage/profile/" . $comment->komentator->foto_profil) }}" alt="" width="30" height="30" /> 
                      </div>
                      <div class="col-sm-11">
                        <div class="d-flex">
                          <span class="fw-bold me-1">{{ $comment->komentator->username }}</span>
                          <span>{{ $comment->komentar }}</span>
                        </div>
                        <div style="font-size: .8rem;">{{ date('d M Y', strtotime($comment->created_at)) }}</div>
                      </div>
                    </div>
                  @endforeach
                </div> 
        
                <hr> 
                <form method="post" action="{{ route('postingan.komentar') }}" class="input-group"> 
                    @csrf
                    <input type="hidden" name="idPostingan" name="idPostingan" value="{{ $post->id }}">
                    <input class="form-control form-control-sm" type="text" id="komen" name="komen" placeholder="Tulis Komentar"> 
                    <button class="btn btn-primary" type="submit">Kirim</button> 
                </form> 
            </div> 
          </div>
        </div>
    </div>
</div>
@endsection
