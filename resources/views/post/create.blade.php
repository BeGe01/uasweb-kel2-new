@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex flex-row align-items-center ">
                    <span>Buat Postingan</span>
                </div>

                <div class="card-body">
                    <form method="post" action="{{ route('postingan.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="fotoPostingan" class="form-label">Pilih foto</label>
                            <input class="form-control" type="file" id="fotoPostingan" name="fotoPostingan">
                        </div>
    
                        <div class="mb-3">
                            <label for="caption" class="form-label">Caption</label>
                            <textarea class="form-control" id="caption" name="caption" rows="3"></textarea>
                        </div>
    
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" type="submit">Posting</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
