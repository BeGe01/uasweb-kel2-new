@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="d-grid gap-2 mb-3">
                <a class="btn btn-primary" href="/postingan/create">Buat Postingan</a>
            </div>
            @if ($user->jumlahFollowing()==0)
            <div class="card mt-4">
                <div class="card-header d-flex flex-row align-items-center fs-5">
                    Belum mengikuti siapapun
                </div>
                <div class="card-body text-center">
                    Anda belum mengikuti siapapun. Ayo ikuti temanmu untuk melihat postingan mereka!
                </div>
            </div>
            @endif
            @foreach ($posts as $index => $post)
                <div class="card mt-4">
                    <div class="card-header d-flex flex-row align-items-center ">
                        <img src="{{ asset("storage/profile/" . $post->foto_profil)}}" alt="" width="30" height="30" />
                        <span class="ms-2">{{ $post->username }}</span>
                    </div>

                    <div class="card-body text-center">
                        <img src="{{ asset("storage/post/" . $post->foto_postingan) }}" width="100%" alt="" />

                        {{-- <div class="d-flex flex-column text-start">
                            <span>Disukai oleh <span style="font-weight: bolder">{{ $post->likes }} Orang</span></span>
                        </div> --}}

                        <div class="d-flex flex-row text-start">
                            <strong>{{ $post->username }}</strong>
                            <span class="ms-2">{{ $post->caption }}</span>
                        </div>

                        <div class="d-flex flex-column text-start text-secondary">
                            <a style="text-decoration: none; color: black" href="{{ route('postingan.detail', ['id' => $post->id]) }} "><strong>Lihat komentar...</strong></a>
                            <span style="font-size: 12px">{{ date('d M Y', strtotime($post->created_at)) }}</span>
                        </div>

                        <hr>
                        <form method="post" action="{{ route('postingan.komentar') }}" class="input-group">
                            @csrf
                            <input type="hidden" name="idPostingan" name="idPostingan" value="{{ $post->id }}">
                            <input class="form-control form-control-sm" type="text" id="komen" name="komen" placeholder="Tulis Komentar"> 
                            <button class="btn btn-primary" type="submit">Kirim</button>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
