@extends('layouts.admin')

@section('content')
{{-- {{dd($user->following())}} --}}
<div class="container">
    <div class="row justify-content-center">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Detail Pengguna</h1> 
        </div>
        <div class="col">
                <div class="card">
                    <div class="card-header">
                        <a href="/admin/pengguna" class="btn btn-primary btn-sm float-start"><span data-feather="chevron-left"></span>Kembali</a>
                    </div>
                    <div class="card-body">
                        <div class="container fs-5">
                            <div class="row">
                                <div class="col-2">Foto Profil: </div>
                                <div class="col-8"><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#foto_profil">Foto</button></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-2">Username: </div>
                                <div class="col-8">{{$user->username}}</div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-2">Nama: </div>
                                <div class="col-8">{{$user->name}}</div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-2">Bio: </div>
                                <div class="col-8">{{$user->bio}}</div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-2">Followers: </div>
                                <div class="col-8"><a href="#" data-bs-toggle="modal" data-bs-target="#modal_followers">{{$user->jumlahFollowers()}}</a></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-2">Following: </div>
                                <div class="col-8"><a href="#" data-bs-toggle="modal" data-bs-target="#modal_following">{{$user->jumlahFollowing()}}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="foto_profil" tabindex="-1" aria-labelledby="foto_profil" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto {{$user->username}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <img src="{{ asset('storage/profile/'.$user->foto_profil) }}" class="img-fluid" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Followers -->
<div class="modal fade" id="modal_followers" tabindex="-1" aria-labelledby="foto_profil" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Daftar Followers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                        </tr>
                    </thead>
                    @foreach ($followers as $index => $follower)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td><a href="./{{ $follower }}">{{ App\Models\Follow::username($follower)}}</a></td>
                    </td>
                    @endforeach
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Following -->
<div class="modal fade" id="modal_following" tabindex="-1" aria-labelledby="foto_profil" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Daftar Following</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                        </tr>
                    </thead>
                    @foreach ($followings as $index => $following)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td><a href="./{{ $following }}">{{ App\Models\Follow::username($following)}}</a></td>
                    </td>
                    @endforeach
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



@endsection
