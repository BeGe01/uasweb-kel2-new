@extends('layouts.admin')

@section('content')
{{-- {{dd($user->following())}} --}}
<div class="container">
    <div class="row justify-content-center">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Tambah Pengguna</h1> 
        </div>
        <div class="col">
                <div class="card">
                    <div class="card-header">
                        <a href="/admin/pengguna" class="btn btn-primary btn-sm float-start"><span data-feather="chevron-left"></span>Kembali</a>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <form action="store" method="post" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                <div class="row mb-2">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" name="username" id="username" class="form-control @error('username') is-invalid @enderror" placeholder="Masukkan Username Anda" value="{{ old('username') }}" autofocus>
                                            @error('username')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name">Nama</label>
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama Anda" value="{{ old('name') }}">
                                            @error('name')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="bio">Bio</label>
                                            <textarea name="bio" id="bio" cols="30" rows="2" class="form-control  @error('bio') is-invalid @enderror">{{ old('bio') }}</textarea>
                                            @error('bio')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
                                            @error('password')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="foto_profil">Foto Profil</label>
                                            <input type="file" name="foto_profil" id="foto_profil" class="form-control @error('foto_profil') is-invalid @enderror">
                                            @error('foto_profil')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-sm mt-2">Tambah</button>
                                <a href="/admin/pengguna" class="btn btn-outline-secondary btn-sm mt-2">Batal</a>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>


@endsection
