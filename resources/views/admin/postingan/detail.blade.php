@extends('layouts.admin')

@section('content')
{{-- {{dd($user->following())}} --}}
<div class="container">
    <div class="row justify-content-center">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Detail Postingan</h1> 
        </div>
        <div class="col">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show">
                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                        {{ Session::get('success') }}
                        @php
                            Session::forget('success');
                        @endphp
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('failed'))
                    <div class="alert alert-danger alert-dismissible fade show">
                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                        {{ Session::get('failed') }}
                        @php
                            Session::forget('failed');
                        @endphp
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <a href="/admin/postingan" class="btn btn-primary btn-sm float-start"><span data-feather="chevron-left"></span>Kembali</a>
                    </div>
                    <div class="card-body">
                        <div class="container fs-5">
                            <div class="row">
                                <div class="col-2">Foto Postingan: </div>
                                <div class="col-8"><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#foto_postingan">Foto</button></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-2">Caption: </div>
                                <div class="col-8">{{$postingan->caption}}</div>
                            </div>
                            <hr>
                            <div class="row">
                                <table class="table table-sm table-hover">
                                    <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Komentar</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    @foreach ($komentars as $index => $komentar)
                                    <tr>
                                        <td><a href="./{{ $komentar->id_komentator }}">{{ $komentar->komentator->username }}</a></td>
                                        <td>{{ $komentar->komentar }}</td>
                                        <td><a href="/admin/postingan/delete_komentar/{{ $komentar->id }}" class="btn btn-outline-danger btn-sm">Hapus</a></td>
                                    </td>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="foto_postingan" tabindex="-1" aria-labelledby="foto_profil" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Postingan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <img src="{{ asset('storage/post/'.$postingan->foto_postingan) }}" class="img-fluid" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



@endsection
