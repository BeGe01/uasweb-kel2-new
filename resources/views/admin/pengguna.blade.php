@extends('layouts.admin')

@section('content')
{{-- {{dd($users)}} --}}
<div class="container">
    <div class="row justify-content-center">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Pengguna</h1> 
        </div>
        <div class="col">
            @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show">
                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                        {{ Session::get('success') }}
                        @php
                            Session::forget('success');
                        @endphp
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(Session::has('failed'))
                    <div class="alert alert-danger alert-dismissible fade show">
                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                        {{ Session::get('failed') }}
                        @php
                            Session::forget('failed');
                        @endphp
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h4 style="display: inline;" class="card-title"></h4>
                        <a href="pengguna/create" class="btn btn-primary btn-sm float-end">Tambah Pengguna</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-sm table-hover align-middle">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Following</th>
                                        <th>Followers</th>
                                        <th>Detail</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $index => $user)
                                        <tr>
                                            <td>{{$index+1}}</td>
                                            <td>{{ strtolower($user->username) }}
                                                <br><span class="text-muted fst-italic">{{ucwords($user->name)}}</span>
                                            </td>
                                            <td class="" >{{ $user->jumlahFollowing() }}</td>
                                            <td>{{ $user->jumlahFollowers() }}</td>
                                            <td><a href="pengguna/detail/{{ $user->id }}" class="btn btn-success btn-sm">Detail</a></td>
                                            {{-- <td><button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#foto{{$user->id}}">Detail</button></td> --}}
                                            <td>
                                                <a href="pengguna/edit/{{ $user->id }}" class="btn btn-outline-primary btn-sm">Ubah</a>
                                                <a href="pengguna/delete/{{ $user->id }}" class="btn btn-outline-danger btn-sm">Hapus</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>



@endsection
