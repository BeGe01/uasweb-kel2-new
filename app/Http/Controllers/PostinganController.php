<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Postingan;
use App\Models\Komentar;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PostinganController extends Controller
{
    public function index(){
        return view('post.create');
    }

    public function store(Request $request) {
        $max_id = DB::table('postingan')->max('id');
        $max_id += 1;
        $filename = $max_id."_".Auth::user()->username . "." . $request->fotoPostingan->extension();
        Storage::putFileAs('public/post', $request->fotoPostingan, $filename);
        Postingan::updateOrCreate(
            ['id' => $request->id],
            [
                'foto_postingan' => $filename,
                'caption' => $request->caption,
                'likes' => 0,
                'id_poster' => Auth::user()->id,
            ]
        );

        return redirect()->route('profile', ['id' => Auth::user()->id]);
    }

    public function detail($id){
        $post = Postingan::findOrFail($id);
        $comments = $post->comments;
        $user = $post->poster;
        $data = [
            'post' => $post,
            'comments' => $comments,
            'user' => $user,
        ];

        return view('post.detail', $data);
    }

    public function komen(Request $request){
        $request->validate([
            'komen' => 'required',
        ]);

        Komentar::create(
            [
                'komentar' => $request['komen'],
                'id_postingan' => $request['idPostingan'],
                'id_komentator' => Auth::user()->id,
            ]
        );

        return redirect()->route('postingan.detail', ['id' => $request['idPostingan']]);
    }
}
