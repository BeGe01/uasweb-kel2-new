<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Postingan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index($id){
        $user = User::findOrFail($id);
        $list_fwing = $user->list_following;
        $list_fwers = $user->list_followers;
        $data = [
            'user' => $user,
            'followers' => $user->jumlahFollowers(),
            'following' => $user->jumlahFollowing(),
            'list_following' => $list_fwing,
            'list_follower' => $list_fwers,
            'posts' => Postingan::where('id_poster', $user->id)->orderBy('created_at','DESC')->get(),
        ];

        return view('user.index', $data);
    }

    public function follow($id){
        $user = User::findOrFail($id);
        if(Follow::where('id_pengguna',Auth::user()->id)
        ->where('id_following',$id)->get()->count()==0)
        {
            Follow::create([
                'id_pengguna' => Auth::user()->id,
                'id_following' => $user->id
            ]);
        }
        
        return back();
    }

    public function unfollow($id){
        $follow = Follow::where('id_pengguna',Auth::user()->id)->where('id_following',$id)->delete();
        return back();
    }

    public function edit($id){
        $user = User::findOrFail($id);
        $data = [
            'user' => $user,
        ];

        return view('user.edit', $data);
    }

    public function update(Request $request){
        $request->validate([
            'foto_profil' => "mimes:jpg,png|max:1024",
        ]);

        $data = [
            'name' => $request->name,
            'bio' => $request->bio,
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        if($request->filled('password'))
        {
            $data += [
                'password' => Hash::make($request->input('password')),
            ];
        }

        if($request->hasFile('foto_profil'))
        {
            $filename = $request->username . '.' . $request->foto_profil->getClientOriginalExtension();
            Storage::putFileAs('public/profile', $request->foto_profil, $filename);
            $data += [
                'foto_profil' => $filename,
            ];
        }
        DB::table('users')->where('id', $request->input('id'))->update($data);
        return redirect()->route('profile', ['id' => Auth::user()->id])->with('success', 'Data berhasil diubah.');
    }
}
