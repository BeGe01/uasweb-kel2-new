<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Postingan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = User::findOrFail(Auth::user()->id);
        $followers = $user->following();
        $posts = DB::table('postingan')
        ->whereIn('id_poster', $followers)
        ->leftJoin('users', 'postingan.id_poster', '=', 'users.id')
        ->select('postingan.id', 'users.username', 'users.name', 'postingan.foto_postingan', 'postingan.caption', 'users.foto_profil','postingan.likes', 'postingan.created_at')
        ->orderBy('created_at','DESC')
        ->get();
        $data = [
            'user' => $user,
            'posts' => $posts,
        ];
        return view('home', $data);
    }

    public function search(Request $request){
        $data = [
            'users' =>User::where('username', 'LIKE', '%'. $request['query'] .'%')->orWhere('name', 'LIKE', '%'. $request['query'] .'%')->get(),
        ];

        return view('search', $data);
    }
}
