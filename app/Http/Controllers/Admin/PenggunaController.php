<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class PenggunaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            //query pertama
            'users' => User::all()
        ];
        return view('admin.pengguna', $data);
    }

    public function create()
    {
        return view('admin.pengguna.create');
    }

    public function store(Request $request){
        $request->validate([
            'username' => "required|unique:users,username",
            'name' => "required",
            'password' => "required",
            'foto_profil' => "mimes:jpg,png|max:1024",
        ]);
        $filename = $request->username . '.' . $request->foto_profil->getClientOriginalExtension();
        Storage::putFileAs('public/profile', $request->foto_profil, $filename);
        User::create([
            'username' => $request->username,
            'name' => $request->name,
            'bio' => $request->bio,
            'password' => Hash::make($request->password),
            'foto_profil' => $filename,
        ]);
        return redirect()->route("admin.pengguna")->with('success', 'Pengguna berhasil ditambahkan.');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $data = [
            'user' => $user,
        ];
        return view('admin.pengguna.edit',$data);
    }

    public function update(Request $request){
        $request->validate([
            'foto_profil' => "mimes:jpg,png|max:1024",
        ]);

        $data = [
            'name' => $request->name,
            'bio' => $request->bio,
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        if($request->filled('password'))
        {
            $data += [
                'password' => Hash::make($request->input('password')),
            ];
        }

        if($request->hasFile('foto_profil'))
        {
            $filename = $request->username . '.' . $request->foto_profil->getClientOriginalExtension();
            Storage::putFileAs('public/profile', $request->foto_profil, $filename);
            $data += [
                'foto_profil' => $filename,
            ];
        }
        DB::table('users')->where('id', $request->input('id'))->update($data);
        return redirect()->route("admin.pengguna")->with('success', 'Data berhasil diubah.');
    }

    public function detail($id)
    {
        $user = User::findOrFail($id);
        $followers = $user->followers();
        $followings = $user->following();
        $data = [
            'user' => $user,
            'followers' => $followers,
            'followings' => $followings
        ];
        return view("admin.pengguna.detail", $data);
    }

    public function delete($id)
    {

        $user = User::findOrFail($id);
        $user -> delete();
        return back()->with('success', 'Pengguna berhasil dihapus.');
        
    }
}
