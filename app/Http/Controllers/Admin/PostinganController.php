<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Komentar;
use App\Models\Postingan;
use App\Models\User;

class PostinganController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'postingans' => Postingan::all()
        ];
        return view('admin.postingan', $data);
    }

    public function detail($id)
    {
        $postingan = Postingan::findOrFail($id);
        $komentars = $postingan->comments;
        $data = [
            'postingan' => $postingan,
            'komentars' => $komentars,
        ];
        return view("admin.postingan.detail", $data);
    }

    public function delete($id)
    {

        $postingan = Postingan::findOrFail($id);
        $postingan -> delete();
        return back()->with('success', 'Postingan berhasil dihapus.');
        
    }

    public function delete_komentar($id)
    {

        $komentar = Komentar::findOrFail($id);
        $komentar -> delete();
        return back()->with('success', 'Komentar berhasil dihapus.');
        
    }
}
