<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'name',
        'bio',
        'foto_profil',
        'password',
    ];

    public function following()
    {
        return $this->hasMany(Follow::class,'id_pengguna', 'id')->pluck('id_following'); // Return ID Following
    }

    public static function isFollowing($id){
        if(Follow::where('id_pengguna',Auth::user()->id)->where('id_following',$id)->get()->count()==0){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function followers()
    {
        return $this->hasMany(Follow::class,'id_following', 'id')->pluck('id_pengguna'); // Return ID Followers
    }
    public function list_followers()
    {
        return $this->hasMany(Follow::class,'id_following', 'id');
    }

    public function list_following()
    {
        return $this->hasMany(Follow::class,'id_pengguna', 'id');
    }


    public function jumlahFollowing()
    {
        return $this->hasMany(Follow::class,'id_pengguna', 'id')->count(); // Return jumlah following
    }

    public function jumlahFollowers()
    {
        return $this->hasMany(Follow::class,'id_following', 'id')->count(); // Return jumlah followers
    }

    public function likes()
    {
        return $this->hasMany(Likes::class,'id_pengguna', 'id')->pluck('id_postingan'); // Return ID Postingan yang di like
    }

    public static function allData()
    {
        return DB::table("users")->get();
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
}
