<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Postingan extends Model
{
    use HasFactory;
    protected $table = 'postingan';
    protected $fillable = [
        'foto_postingan',
        'caption',
        'likes',
        'id_poster'

    ];

    public function poster()
    {
        return $this->belongsTo(User::class,'id_poster');
    }

    public function comments()
    {
        return $this->hasMany(Komentar::class,'id_postingan', 'id');
    }

    public function likes()
    {
        return $this->hasMany(Likes::class,'id_postingan', 'id');
    }

    public function jumlahKomentar()
    {
        return $this->hasMany(Komentar::class,'id_postingan', 'id')->count();
    }
    
    public function jumlahLikes()
    {
        return $this->hasMany(Likes::class,'id_postingan', 'id')->count();
    }

    public static function allData()
    {
        // query builder
        return DB::table("postingan")->get();
    }
}
